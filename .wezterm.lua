local wezterm = require("wezterm")
local config  = {}

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config      = {
  adjust_window_size_when_changing_font_size = false,
  animation_fps                              = 60,
  audible_bell                               = "Disabled",
  automatically_reload_config                = true,
  check_for_updates                          = false,
  color_scheme                               = "Catppuccin Mocha",
  default_cursor_style                       = "BlinkingBlock",
  default_prog                               = { "/usr/bin/bash" },
  enable_scroll_bar                          = false,
  enable_wayland                             = true,
  font                                       = wezterm.font("Cascadia Code"),
  font_size                                  = 14,
  harfbuzz_features                          = { "calt=1", "clig=1", "liga=1" },
  hide_tab_bar_if_only_one_tab               = true,
  hyperlink_rules                            = wezterm.default_hyperlink_rules(),
  initial_cols                               = 120,
  initial_rows                               = 30,
  scrollback_lines                           = 8192,
  switch_to_last_active_tab_when_closing_tab = true,
  tab_bar_at_bottom                          = true,
  tab_max_width                              = 80,
  unicode_version                            = 15,
  use_fancy_tab_bar                          = false,
  window_close_confirmation                  = "NeverPrompt",
  window_padding                             = { left = 0, right = 0, top = 0, bottom = 0 },
}

config.keys = {
  {
    key    = "|",
    mods   = "CTRL|SHIFT",
    action = wezterm.action.SplitHorizontal({ domain = "CurrentPaneDomain" }),
  },
  {
    key    = "\\",
    mods   = "CTRL|ALT",
    action = wezterm.action.SplitVertical({ domain = "CurrentPaneDomain" }),
  },
}

return config
