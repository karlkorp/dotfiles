#!/bin/bash

pacman -Sy archlinux-keyring
pacman-key --init
pacman-key --populate archlinux
pacman -Sy
pacman -Sc

read -r -p "HOST NAME: " hostname
read -r -p "USER NAME: " username

if [ -z "$hostname" ] || [ -z "$username" ]
then
  echo "ERROR!" && return
fi

ln -fsv /usr/share/zoneinfo/Europe/Moscow /etc/localtime
hwclock --systohc

sed -i 's/#en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/g' /etc/locale.gen

{
  echo "FONT=cyr-sun16"
  echo "KEYMAP=us"
} > /etc/vconsole.conf

echo "LANG=en_US.UTF-8" > /etc/locale.conf

locale-gen

echo "$hostname" > /etc/hostname

{
  echo "127.0.0.1 localhost"
  echo "::1       localhost"
  echo "127.0.1.1 $hostname.localdomain $hostname"
} > /etc/hosts

mkinitcpio -P

echo "SET THE ROOT PASSWORD"
passwd

echo "NEW USER: $username"
useradd -m -s /bin/bash "$username"

usermod -aG audio   "$username"
usermod -aG disk    "$username"
usermod -aG input   "$username"
usermod -aG lp      "$username"
usermod -aG optical "$username"
usermod -aG scanner "$username"
usermod -aG storage "$username"
usermod -aG video   "$username"
usermod -aG wheel   "$username"

echo "SET THE $username PASSWORD"
passwd "$username"

pacman -S --needed              \
       gst-plugin-pipewire      \
       jack-example-tools       \
       pipewire                 \
       pipewire-alsa            \
       pipewire-audio           \
       pipewire-ffado           \
       pipewire-jack            \
       pipewire-pulse           \
       pipewire-roc             \
       pipewire-session-manager \
       pipewire-v4l2            \
       pipewire-x11-bell        \
       pipewire-zeroconf        \
       qpwgraph                 \
       wireplumber

pacman -S --needed            \
       acpi                   \
       acpi_call-lts          \
       avahi                  \
       bash-completion        \
       bc                     \
       bind                   \
       btrfs-progs            \
       cifs-utils             \
       cloc                   \
       criu                   \
       cronie                 \
       crosstool-ng           \
       cryptsetup             \
       curl                   \
       detox                  \
       devtools               \
       dhclient               \
       dhcpcd                 \
       dialog                 \
       discount               \
       dosfstools             \
       e2fsprogs              \
       efibootmgr             \
       ethtool                \
       exfatprogs             \
       f2fs-tools             \
       fdupes                 \
       file                   \
       glfw                   \
       glusterfs              \
       gptfdisk               \
       grub                   \
       haveged                \
       hdf5-openmpi           \
       icu                    \
       iproute2               \
       iw                     \
       iwd                    \
       jfsutils               \
       lazygit                \
       less                   \
       libtool                \
       libusb                 \
       lm_sensors             \
       lnav                   \
       lsb-release            \
       lvm2                   \
       man-pages              \
       mdadm                  \
       modemmanager           \
       mtools                 \
       ncdu                   \
       netctl                 \
       network-manager-sstp   \
       networkmanager         \
       networkmanager-openvpn \
       networkmanager-pptp    \
       networkmanager-vpnc    \
       nfs-utils              \
       nilfs-utils            \
       nmon                   \
       nss-mdns               \
       ntfs-3g                \
       os-prober              \
       pacman-contrib         \
       parallel               \
       parted                 \
       pass                   \
       patch                  \
       plocate                \
       polkit                 \
       r8168-lts              \
       readline               \
       reiserfsprogs          \
       rlwrap                 \
       rmlint                 \
       rng-tools              \
       s-nail                 \
       sd                     \
       slop                   \
       snapper                \
       sudo                   \
       sysstat                \
       termdown               \
       texinfo                \
       time                   \
       trace-cmd              \
       tree                   \
       util-linux             \
       wget                   \
       which                  \
       wireless_tools         \
       wpa_supplicant         \
       xfsprogs

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub
grub-mkconfig -o /boot/grub/grub.cfg

sudo systemctl enable NetworkManager.service
sudo systemctl enable dhcpcd.service
sudo systemctl enable iwd.service

echo       "Base system installation was finished!"
read -r -p "Press 'enter' to exit..."
sleep 5 && exit
