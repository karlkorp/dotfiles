#!/bin/bash

if [ "$USER" != "root" ]
then
  if ! grep -i "$USER" /etc/sudoers &> /dev/null
  then
    chmod +w                                                      /etc/sudoers
    sed   -i "/root*.ALL=(ALL:ALL) ALL/a $USER ALL=(ALL:ALL) ALL" /etc/sudoers
    chmod -w                                                      /etc/sudoers
    cat                                                           /etc/sudoers
  else
    echo "$USER already in /etc/sudoers"
  fi
fi

exit
