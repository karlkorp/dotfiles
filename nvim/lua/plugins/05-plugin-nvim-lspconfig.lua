local servers = {
  "awk_ls",
  "bashls",
  "clangd",
  "cmake",
  "eslint",
  "gopls",
  "jsonls",
  "lua_ls",
  "pylsp",
  "pyright",
  "rust_analyzer",
  "taplo",
  "texlab",
  "ts_ls",
}

local tools = {
  "autoflake",
  "autopep8",
  "black",
  "clang-format",
  "cmakelint",
  "codespell",
  "eslint_d",
  "flake8",
  "latexindent",
  "luacheck",
  "mypy",
  "prettier",
  "prettierd",
  "pylint",
  "revive",
  "ruff",
  "shellcheck",
  "shellharden",
  "shfmt",
  "staticcheck",
  "stylua",
  "vale",
  "yapf",
}

require("lspsaga").setup({})

require("mason").setup({})

require("mason-lspconfig").setup({
  automatic_installation = true,
  ensure_installed       = servers,
})

require("mason-tool-installer").setup({
  auto_update      = true,
  ensure_installed = tools,
  run_on_start     = true,
})

local lspconfig    = require("lspconfig")
local capabilities = require("cmp_nvim_lsp").default_capabilities(
  vim.lsp.protocol.make_client_capabilities()
)

for _, server in ipairs(servers) do
  lspconfig[server].setup({ capabilities = capabilities })
end

lspconfig["lua_ls"].setup({
  settings = { Lua = { diagnostics = { globals = { "vim" } } } },
})

vim.api.nvim_set_keymap(
  "n", "<LEADER>ca", ":lua vim.lsp.buf.code_action()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>gc", ":lua vim.lsp.buf.declaration()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>gf", ":lua vim.lsp.buf.definition()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>gi", ":lua vim.lsp.buf.implementation()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>gr", ":lua vim.lsp.buf.references()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>rn", ":lua vim.lsp.buf.rename()<CR>",
  {
    noremap = true,
    silent  = true,
  }
)
