local actions = require("telescope.actions")

require("telescope").setup({
  defaults        = {
    find_command      = {
      "fd",
      "--color",
      "--extension",
      "--hidden",
      "--ignore-case",
    },
    mappings          = { n = { ["q"] = actions.close } },
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--column",
      "--hidden",
      "--line-number",
      "--no-heading",
      "--smart-case",
      "--with-filename",
    },
  },
  extensions      = {
    file_browser = {
      theme = "dropdown",
    },
    fzf          = {
      case_mode               = "smart_case",
      fuzzy                   = true,
      override_file_sorter    = true,
      override_generic_sorter = true,
    },
  },
  layout_strategy = "vertical",
  pickers         = {
    find_files = { hidden = true, theme = "dropdown" },
    live_grep  = { theme = "dropdown" },
  },
  use_less        = true,
})

require("telescope").load_extension("file_browser")
require("telescope").load_extension("fzf")

vim.api.nvim_set_keymap(
  "n", "<LEADER>fb", ":Telescope file_browser<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>ff", ":Telescope find_files<CR>",
  {
    noremap = true,
    silent  = true,
  }
)

vim.api.nvim_set_keymap(
  "n", "<LEADER>fg", ":Telescope live_grep<CR>",
  {
    noremap = true,
    silent  = true,
  }
)
