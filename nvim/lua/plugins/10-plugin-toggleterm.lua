require("toggleterm").setup({
  auto_scroll   = true,
  close_on_exit = true,
  direction     = "float",
})

vim.api.nvim_set_keymap(
  "n", "<F1>", ":ToggleTerm<CR>",
  {
    noremap = true,
    silent  = true,
  }
)
