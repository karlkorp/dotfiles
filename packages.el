;;; Package --- packages.el
;;; Commentary:
;;; GNU Emacs external packages configuration file

;;; -*- lexical-binding: t -*-
;;; -*- no-byte-compile: t -*-

;;; Code:
(require 'package nil :noerror)
(setq-default package-check-signature   nil )
(setq-default package-enable-at-startup nil )
(setq-default package-native-compile    t   )
(setq-default package-quickstart        t   )

(add-to-list 'package-archives '( "gnu"    . "https://elpa.gnu.org/packages/"  ) t )
(add-to-list 'package-archives '( "melpa"  . "https://melpa.org/packages/"     ) t )
(add-to-list 'package-archives '( "nongnu" . "https://elpa.nongnu.org/nongnu/" ) t )

(package-initialize)

(unless package-archive-contents
  (ignore-errors (package-refresh-contents) ) )

(unless (package-installed-p 'use-package)
  (package-install 'use-package) )

(require 'use-package nil :noerror)
(setq-default use-package-always-ensure        t )
(setq-default use-package-compute-statistics   t )
(setq-default use-package-enable-imenu-support t )
(setq-default use-package-expand-minimally     t )

(use-package ace-window
  :bind ("M-o" . ace-window)
  :init (ace-window-display-mode) )

(use-package async
  :config (autoload 'dired-async-mode "dired-async.el" nil t)
  :init
  (dired-async-mode            )
  (async-bytecomp-package-mode ) )

(use-package company
  :bind
  ( :map company-active-map
    ( "<tab>" . company-indent-or-complete-common )
    ( "C-d"   . company-show-doc-buffer           )
    ( "C-n"   . company-select-next               )
    ( "C-p"   . company-select-previous           ) )
  :hook (prog-mode . company-mode)
  :init
  (setq-default company-dabbrev-downcase           nil   )
  (setq-default company-idle-delay                 0     )
  (setq-default company-minimum-prefix-length      7     )
  (setq-default company-selection-wrap-around      t     )
  (setq-default company-show-quick-access         'left  )
  (setq-default company-tooltip-align-annotations  t     )
  (setq-default company-tooltip-flip-when-above    t     )
  (setq-default company-tooltip-limit              5     )
  (setq-default company-tooltip-margin             3     )
  (setq-default company-tooltip-offset-display    'lines ) )

(use-package consult
  :bind
  ( ( "C-s"   . consult-line                )
    ( "C-x b" . consult-buffer              )
    ( "M-s f" . consult-find                )
    ( "M-s r" . consult-ripgrep             )
    ( "M-y"   . consult-yank-from-kill-ring ) )
  :config
  (setq-default xref-show-definitions-function 'consult-xref )
  (setq-default xref-show-xrefs-function       'consult-xref ) )

(use-package editorconfig
  :config (editorconfig-mode)
  :if (executable-find "editorconfig") )

(use-package emacsql)

(use-package embark
  :bind ("C-." . embark-act) )

(use-package embark-consult
  :after (consult embark)
  :hook (embark-collect-mode . consult-preview-at-point-mode) )

(use-package exec-path-from-shell
  :config (exec-path-from-shell-initialize) )

(use-package expand-region
  :bind ("C-=" . er/expand-region) )

(use-package flycheck
  :init (global-flycheck-mode) )

(use-package gap-mode
  :config
  (autoload 'gap-mode "gap-mode" "Gap editing mode" t  )
  (setq-default gap-executable (executable-find "gap") )
  :if (executable-find "gap")
  :mode
  ( ( "\\.g\\'"   . gap-mode )
    ( "\\.gap\\'" . gap-mode )
    ( "\\.gd\\'"  . gap-mode )
    ( "\\.gi\\'"  . gap-mode ) ) )

(use-package glsl-mode
  :config (autoload 'glsl-mode "glsl-mode" nil t)
  :if (executable-find "glslangValidator")
  :mode
  ( ( "\\.frag\\'" . glsl-mode )
    ( "\\.geom\\'" . glsl-mode )
    ( "\\.glsl\\'" . glsl-mode )
    ( "\\.vert\\'" . glsl-mode ) ) )

(use-package gnuplot
  :config
  (autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot-mode" t )
  (autoload 'gnuplot-mode        "gnuplot" "Gnuplot major mode"            t )
  :if (executable-find "gnuplot")
  :mode ("\\.gp\\'" . gnuplot-mode) )

(use-package go-mode
  :hook (before-save . gofmt-before-save)
  :if (executable-find "go") )

(use-package graphviz-dot-mode
  :if (executable-find "dot") )

(use-package haskell-mode
  :hook
  ( ( haskell-mode . haskell-auto-insert-module-template )
    ( haskell-mode . haskell-setup-outline-mode          )
    ( haskell-mode . interactive-haskell-mode            )
    ( haskell-mode . turn-on-haskell-doc-mode            ) )
  :if (executable-find "ghc") )

(use-package hl-todo
  :hook
  ( ( org-mode  . hl-todo-mode )
    ( prog-mode . hl-todo-mode ) ) )

(use-package iedit)

(use-package js2-mode
  :mode ("\\.js\\'" . js2-mode) )

(use-package json-mode
  :mode ("\\.json\\'" . json-mode) )

(use-package clojure-mode
  :after smartparens-mode
  :hook
  ( ( clojure-mode . cider-mode              )
    ( clojure-mode . smartparens-strict-mode ) )
  :if (executable-find "clojure") )

(use-package cider
  :config
  (defun cider-before-save-hook ()
    "Format buffer before saving (for Clojure)."
    (interactive)
    (when (equal major-mode 'clojure-mode)
      (save-excursion
        (delete-trailing-whitespace )
        (lsp-format-buffer          )
        (recenter                   )
        ) ) )
  (add-hook 'before-save-hook #'cider-before-save-hook)

  (setq-default cider-repl-display-help-banner       nil          )
  (setq-default cider-repl-pop-to-buffer-on-connect 'display-only )
  (setq-default org-babel-clojure-backend           'cider        )
  :if (executable-find "clojure") )

(use-package clojure-mode-extra-font-locking
  :after clojure-mode
  :if (executable-find "clojure") )

(use-package fennel-mode
  :if (executable-find "fennel") )

(use-package geiser
  :config
  (setq-default geiser-active-implementations '(chez) )
  (setq-default geiser-default-implementation 'chez   )
  :if (executable-find "chez") )

(use-package geiser-chez
  :after geiser
  :config (setq-default geiser-chez-binary (executable-find "chez") )
  :if (executable-find "chez") )

(use-package racket-mode
  :config (require 'racket-xp nil :noerror)
  :hook
  ( ( racket-mode      . racket-unicode-input-method-enable )
    ( racket-mode      . racket-xp-mode                     )
    ( racket-repl-mode . racket-unicode-input-method-enable ) )
  :if (executable-find "racket")
  :mode ("\\.rkt\\'" . racket-mode) )

(use-package slime
  :config
  (require 'slime-autoloads nil :noerror)
  (setq-default common-lisp-style-default "sbcl")
  (slime-setup '(slime-asdf slime-fancy slime-indentation slime-references slime-tramp) )
  :if (executable-find "sbcl") )

(use-package logview)

(use-package lsp-mode
  :bind
  ( :map lsp-mode-map
    ("M-<return>" . lsp-rename) )
  :config
  (setq-default lsp-auto-guess-root              t   )
  (setq-default lsp-eldoc-render-all             t   )
  (setq-default lsp-enable-snippet               nil )
  (setq-default lsp-enable-symbol-highlighting   nil )
  (setq-default lsp-headerline-breadcrumb-enable nil )
  :hook
  ( ( bibtex-mode        . lsp )
    ( c++-mode           . lsp )
    ( c-mode             . lsp )
    ( clojure-mode       . lsp )
    ( clojurec-mode      . lsp )
    ( clojurescript-mode . lsp )
    ( go-mode            . lsp )
    ( haskell-mode       . lsp )
    ( js2-mode           . lsp )
    ( latex-mode         . lsp )
    ( lua-mode           . lsp )
    ( python-mode        . lsp )
    ( rustic-mode        . lsp )
    ( sh-mode            . lsp )
    ( tex-mode           . lsp )
    ( typescript-mode    . lsp )
    ( yatex-mode         . lsp ) )
  :if
  (or ( executable-find "bash-language-server"       )
      ( executable-find "clangd"                     )
      ( executable-find "clojure-lsp"                )
      ( executable-find "ghcide"                     )
      ( executable-find "gopls"                      )
      ( executable-find "haskell-language-server"    )
      ( executable-find "lua-language-server"        )
      ( executable-find "pylsp"                      )
      ( executable-find "rust-analyzer"              )
      ( executable-find "texlab"                     )
      ( executable-find "typescript-language-server" ) )
  :init
  (setq-default lsp-clients-clangd-args '( "--background-index"
                                           "--clang-tidy"
                                           "--log=error"
                                           "--pch-storage=memory"
                                           "--pretty"
                                           "-j=4"                 ) )
  (setq-default lsp-keymap-prefix          "C-c l"                  ) )

(use-package lsp-haskell
  :after (haskell-mode lsp)
  :if
  (or (executable-find "ghcide"                  )
      (executable-find "haskell-language-server" ) ) )

(use-package lsp-latex
  :after lsp
  :if (executable-find "texlab") )

(use-package lsp-ui
  :after lsp
  :config
  (lsp-ui-doc-show           )
  (lsp-ui-peek-jump-backward )
  (lsp-ui-peek-jump-forward  )
  (setq-default lsp-ui-doc-position        'bottom )
  (setq-default lsp-ui-sideline-enable      t      )
  (setq-default lsp-ui-sideline-show-hover  nil    )
  :hook (lsp-mode . lsp-ui-mode) )

(use-package lua-mode
  :config (setq-default lua-indent-level 2)
  :if (executable-find "lua") )

(use-package magit
  :bind ("C-x g" . magit-status)
  :if (executable-find "git") )

(use-package marginalia
  :after vertico
  :init (marginalia-mode) )

(use-package markdown-mode
  :config
  (setq-default markdown-display-remote-images        t )
  (setq-default markdown-enable-math                  t )
  (setq-default markdown-fontify-code-blocks-natively t )
  :if (executable-find "markdown")
  :init (setq-default markdown-command "markdown") )

(use-package minions
  :config (setq-default minions-mode-line-lighter "\u2026")
  :init (minions-mode) )

(use-package multiple-cursors
  :bind
  ( ( "C-<"         . mc/mark-previous-like-this )
    ( "C->"         . mc/mark-next-like-this     )
    ( "C-S-c C-S-c" . mc/edit-lines              )
    ( "C-c C-<"     . mc/mark-all-like-this      ) ) )

(use-package orderless
  :config
  (setq-default completion-category-overrides
                '( (file (styles . (basic orderless partial-completion) ) ) ) )
  (setq-default completion-styles
                '(basic flex partial-completion prescient substring) ) )

(use-package org
  :config
  (require 'ob         nil :noerror)
  (require 'ob-clojure nil :noerror)
  (require 'org        nil :noerror)
  (require 'org-tempo  nil :noerror)
  (require 'ox-latex   nil :noerror)
  (unless (file-directory-p org-directory)
    (make-directory org-directory) )
  (org-babel-do-load-languages 'org-babel-load-languages
                               '( ( C          . t )
                                  ( awk        . t )
                                  ( clojure    . t )
                                  ( dot        . t )
                                  ( emacs-lisp . t )
                                  ( gnuplot    . t )
                                  ( java       . t )
                                  ( js         . t )
                                  ( lisp       . t )
                                  ( makefile   . t )
                                  ( maxima     . t )
                                  ( octave     . t )
                                  ( perl       . t )
                                  ( python     . t )
                                  ( scheme     . t )
                                  ( sed        . t )
                                  ( shell      . t ) ) )
  (setq-default org-checkbox-hierarchical-statistics         nil              )
  (setq-default org-confirm-babel-evaluate                   nil              )
  (setq-default org-fontify-done-headline                    t                )
  (setq-default org-fontify-quote-and-verse-blocks           t                )
  (setq-default org-fontify-whole-heading-line               t                )
  (setq-default org-hide-emphasis-markers                    t                )
  (setq-default org-hide-leading-stars                       t                )
  (setq-default org-indent-mode-turns-on-hiding-stars        nil              )
  (setq-default org-list-allow-alphabetical                  t                )
  (setq-default org-outline-path-complete-in-steps           nil              )
  (setq-default org-pretty-entities                          t                )
  (setq-default org-refile-use-outline-path                  t                )
  (setq-default org-return-follows-link                      t                )
  (setq-default org-src-ask-before-returning-to-edit-buffer  nil              )
  (setq-default org-src-fontify-natively                     t                )
  (setq-default org-src-preserve-indentation                 t                )
  (setq-default org-src-tab-acts-natively                    t                )
  (setq-default org-src-window-setup                        'reorganize-frame )
  (setq-default org-startup-folded                           nil              )
  (setq-default org-startup-indented                         t                )
  (setq-default org-startup-with-inline-images               t                ) )

(use-package org-roam
  :after org
  :bind
  ( ( "C-c n c" . org-roam-capture     )
    ( "C-c n f" . org-roam-node-find   )
    ( "C-c n r" . org-roam-node-random )
    (:map org-mode-map
          ( ( "C-c n a" . org-roam-alias-add     )
            ( "C-c n i" . org-roam-node-insert   )
            ( "C-c n l" . org-roam-buffer-toggle )
            ( "C-c n o" . org-id-get-create      )
            ( "C-c n t" . org-roam-tag-add       ) ) ) )
  :config
  (require 'org-roam-export   nil :noerror )
  (require 'org-roam-protocol nil :noerror )
  (org-roam-db-autosync-mode )
  (org-roam-setup            )
  :init
  (setq-default org-roam-completion-everywhere  t              )
  (setq-default org-roam-database-connector    'sqlite-builtin )
  (setq-default org-roam-db-update-on-save      t              )
  (setq-default org-roam-directory              org-directory  )
  (setq-default org-roam-v2-ack                 t              ) )

(use-package org-roam-ui
  :after org-roam
  :config
  (setq-default org-roam-ui-follow         t )
  (setq-default org-roam-ui-open-on-start  t )
  (setq-default org-roam-ui-sync-theme     t )
  (setq-default org-roam-ui-update-on-save t ) )

(use-package org-superstar
  :after org
  :config (setq-default org-superstar-special-todo-items t)
  :hook (org-mode . org-superstar-mode) )

(use-package prescient)

(use-package company-prescient
  :after (company prescient)
  :init (company-prescient-mode) )

(use-package vertico-prescient
  :after (vertico prescient)
  :init (vertico-prescient-mode) )

(use-package projectile
  :bind
  ( :map projectile-mode-map
    ("C-c p" . projectile-command-map) )
  :config
  (setq-default projectile-completion-system 'default         )
  (setq-default projectile-indexing-method   'hybrid          )
  (setq-default projectile-sort-order        'recently-active )
  :if (executable-find "git")
  :init (projectile-mode) )

(use-package python-mode
  :if (executable-find "python") )

(use-package rg
  :if (executable-find "rg")
  :init (rg-enable-default-bindings) )

(use-package rust-mode
  :if (executable-find "rustup") )

(use-package rustic
  :config
  (setq-default lsp-rust-analyzer-cargo-watch-command        "clippy" )
  (setq-default lsp-rust-analyzer-server-display-inlay-hints  t       )
  (setq-default rustic-format-on-save                         t       )
  :if (executable-find "rustup") )

(use-package smartparens
  :config (require 'smartparens-config nil :noerror)
  :hook (prog-mode . smartparens-mode)
  :init (show-smartparens-global-mode) )

(use-package catppuccin-theme
  :if window-system
  :init (load-theme 'catppuccin t nil) )

(use-package ef-themes
  :if window-system)

(use-package modus-themes
  :if window-system)

(use-package toml-mode)

(use-package treesit-auto
  :after lsp
  :config
  (setq-default treesit-auto-install t)
  (global-treesit-auto-mode)
  :if (executable-find "tree-sitter") )

(use-package typescript-mode
  :if (executable-find "tsc") )

(use-package vertico
  :config (setq-default vertico-cycle t)
  :init
  (ido-everywhere -1 )
  (ido-mode       -1 )
  (vertico-mode    1 ) )

(use-package web-mode
  :config
  (setq-default web-mode-code-indent-offset      2 )
  (setq-default web-mode-css-indent-offset       2 )
  (setq-default web-mode-enable-auto-pairing     t )
  (setq-default web-mode-enable-css-colorization t )
  (setq-default web-mode-markup-indent-offset    2 )
  :mode
  ( ( "\\.css\\'"  . web-mode )
    ( "\\.html\\'" . web-mode ) ) )

(use-package wgrep
  :config (setq-default wgrep-auto-save-buffer t) )

(use-package which-key
  :init
  (which-key-mode                     )
  (which-key-setup-minibuffer         )
  (which-key-setup-side-window-bottom ) )

(use-package yaml-mode
  :mode ("\\.yml\\'" . yaml-mode) )

(provide 'packages.el)
;;; packages.el ends here
